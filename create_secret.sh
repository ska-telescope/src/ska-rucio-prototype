#!/usr/bin/env bash

kubectl config set-context --current --namespace=rucio-test
# Robot pem files in HostCreds, User pem files in UserCreds 
HOSTCREDSDIR=/opt/deployed/HostCreds
# CA Certificate location
CA_CERT=/opt/deployed/CACertificates/UKeScienceCA-2B.pem
# Dir for all the CA certs (not just UK)
CA_CERTS=/opt/deployed/AllTheCertificates

# This variable is the rucio helm server deployment's release name
export SERVER_NAME=server
# This variable is the rucio helm daemons deployment's release name
export DAEMONS_NAME=daemons

# Create secrets for the server to consume
# When creating secrets, default key name is the filename. You may optionally set the key name using --from-file=[key=]source

kubectl create secret tls rucio-server.tls-secret --key=$HOSTCREDSDIR/hostkey.pem --cert=$HOSTCREDSDIR/hostcert.pem

kubectl create secret generic $SERVER_NAME-server-hostcert --from-file=hostcert.pem=/$HOSTCREDSDIR/hostcert.pem
kubectl create secret generic $SERVER_NAME-server-hostkey --from-file=hostkey.pem=/$HOSTCREDSDIR/hostkey.pem
kubectl create secret generic $SERVER_NAME-server-cafile --from-file=ca.pem=/$CA_CERT

kubectl create secret generic $SERVER_NAME-auth-hostcert --from-file=hostcert.pem=/$HOSTCREDSDIR/hostcert.pem
kubectl create secret generic $SERVER_NAME-auth-hostkey --from-file=hostkey.pem=/$HOSTCREDSDIR/hostkey.pem
kubectl create secret generic $SERVER_NAME-auth-cafile --from-file=ca.pem=/$CA_CERT

kubectl create secret generic server-rucio-x509up --from-file=proxy-volume=/$HOSTCREDSDIR/x509up_u0

# Create secrets for the daemons to consume
kubectl create secret generic ${DAEMONS_NAME}-fts-cert --from-file=usercert.pem=/$HOSTCREDSDIR/hostcert.pem
kubectl create secret generic ${DAEMONS_NAME}-fts-key --from-file=new_userkey.pem=/$HOSTCREDSDIR/hostkey.pem
kubectl create secret generic ${DAEMONS_NAME}-hermes-cert --from-file=usercert=/$HOSTCREDSDIR/hostcert.pem
kubectl create secret generic ${DAEMONS_NAME}-hermes-key --from-file=userkey=/$HOSTCREDSDIR/hostkey.pem
kubectl create secret generic ${DAEMONS_NAME}-rucio-ca-bundle --from-file=/$CA_CERT
kubectl create secret generic ${DAEMONS_NAME}-rucio-x509up --from-file=x509up=/$HOSTCREDSDIR/x509up_u0

# Client secrets (to be mounted into client pods)
kubectl create secret generic clients-user-cert --from-file=usercert=/$HOSTCREDSDIR/hostcert.pem
kubectl create secret generic clients-user-key --from-file=userkey=/$HOSTCREDSDIR/hostkey.pem
kubectl create secret generic clients-rucio-ca-bundle-test --from-file=ca.pem=/$CA_CERT

kubectl create secret generic clients-admin-cert --from-file=usercert.pem=/$HOSTCREDSDIR/hostcert.pem
kubectl create secret generic clients-admin-key --from-file=userkey.pem=/$HOSTCREDSDIR/hostkey.pem

# Special secrets for the reaper daemon. Reapers needs the whole directory of certificates
mkdir /tmp/reaper-certs
cp $CA_CERTS/* /tmp/reaper-certs/
#cp /etc/grid-security/certificates/*.signing_policy /tmp/reaper-certs/
kubectl create secret generic ${DAEMONS_NAME}-rucio-ca-bundle-reaper --from-file=/tmp/reaper-certs/
kubectl create secret generic clients-user-ca --from-file=/tmp/reaper-certs/
mkdir /tmp/user-certs
cp -r /tmp/reaper-certs/* /tmp/user-certs/
kubectl create secret generic clients-rucio-ca-bundle --from-file=/tmp/user-certs/

rm -rf /tmp/reaper-certs
rm -rf /tmp/user-certs

# We have not deployed the webUI yet, but leaving these in
export INSTANCE=v1
export UI_NAME=ska-rucio-webui-${INSTANCE}
kubectl create secret generic ${UI_NAME}-hostcert --from-file=hostcert.pem=/$HOSTCREDSDIR/hostcert.pem
kubectl create secret generic ${UI_NAME}-hostkey --from-file=hostkey.pem=/$HOSTCREDSDIR/hostkey.pem
kubectl create secret generic ${UI_NAME}-cafile --from-file=ca.pem=/$CA_CERT
