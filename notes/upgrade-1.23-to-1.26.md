## Create special new upgrade image + pod

Rucio’s schema version is stored in the `alembic_version` table. Following guidance from CMS, build an image that looks like the rucio-upgrade image here: https://github.com/dmwm/CMSKubernetes/tree/master/docker/rucio-upgrade after commenting out a few CMS specific lines. You can add a line to the Dockerfile for this image to install the postgres client with 
```
yum install postgresql
```
or install on the fly (described later). We pushed this image to our local registry after tagging as `192.168.50.97:5000/rucio/daemons-ska-upgrade:1.26.2`

Build a deployment-managed pod based on this upgrade image with
```
kubectl create deployment --image=192.168.50.97:5000/rucio/daemons-ska-upgrade:1.26.2 rucio-upgrade -o yaml > upgrade-deployment.yaml
```

## Database upgrade via Alembic

Get the SQL required to manually upgrade the database by jumping in the `upgrade-rucio` pod and running (from the directory with `alembic.ini`):

Check the `alembic.ini` file's `sqlalchemy.url` field and update it to point to your rucio DB.

```
$ alembic upgrade 50280c53117c:head --sql > upgrade-from-1.23-to-1.26.sql
```

Where `50280c53117c` is the version of Alembic corresponding to Rucio 1.23 (from changelogs). Make sure there is a `BEGIN;` and `COMMIT;` in the alembic-generated SQL so that changes will rollback if unsuccessful. 


Inside a pod able to see the rucio postgres database, install a postgres client (upgrade-rucio in this example):

```
[root@upgrade-rucio tmp] yum install postgresql
```

Next, need to edit postgres `search_path` (a list of schemas it checks by default) to include the schema defined for rucio (`test` in this example) in the `rucio.cfg` file. 

Then open a postgresql shell connected to the rucio postgres database:

```
[root@upgrade-rucio tmp]# psql --host rucio-postgresql -U rucio -d rucio -p 5432
```

where `rucio-postgresql` is the pod name.

Finally, issue the query:

```
alter user rucio set search_path = public, test;
```

To upgrade, manually edit the outputted sql file (`upgrade-from-1.23-to-1.26.sql`) until it runs (note: had to comment out some “`DROP`” type lines as the resource they were referring to didn’t already exist i.e. `DROP TYPE "REQUESTS_HISTORY_STATE_CHK";` and `DROP TYPE "REQUESTS_STATE_CHK”;`)

```
[root@upgrade-rucio tmp]# psql --host rucio-postgresql -U rucio -d rucio -p 5432 -f upgrade-from-1.23-to-1.26.sql
```

Note: This script is not expected to take long to run, if any single command appears to get stuck there could be a stuck process with a lock on the DB that prevents the upgrade script from executing. This can require a manual debugging process to find and kill the process that holds the lock; see [this](https://stackoverflow.com/questions/22896496/alembic-migration-stuck-with-postgresql) article. In theory, disabling the daemons first should also circumvent this issue.

## Helm upgrade

These commands install/upgrade helm releases named `server` and `daemons` in the `rucio-test` k8s namespace with helm chart version 1.26.0.
```
# (Ensure Helm repos are up to date)
$ helm repo add rucio https://rucio.github.io/helm-charts
$ helm repo update
```
```
helm upgrade --install server --namespace rucio-test rucio/rucio-server --version 1.26.0  -f server.yaml
helm upgrade daemons --namespace rucio-test rucio/rucio-daemons --version 1.26.0 -f daemons.yaml
```

## Fix issue arising from https://github.com/rucio/rucio/issues/3994 

Inside the rucio server auth pod (`server-rucio-server-auth-cbbf6b954-j9xxv` in this example), amend `get_auth_token_user_pass` function at `/usr/local/lib/python3.6/site-packages/rucio/core/authentication.py` and add:

```
raise CannotAuthenticate(db_password + " " +  hashlib.sha256(salted_password).hexdigest())
```

at line 96. This effectively creates a breakpoint showing the password stored in the database using the older salting method, and the password from the new salting method respectively. A server restart is required for this to take effect:

```
httpd -k graceful
```

Now, issuing a curl to the auth server with a known username will return a `CannotAuthenticate` error with the newly salted password included, e.g.:

```
$ curl -i http://server-rucio-server-auth/auth/userpass -H "X-Rucio-Account: root" -H "X-Rucio-Username: srcdev" -H "X-Rucio-Password: <insert-password-here>"
```

Take a note of the both the older and newer salted password string for this user.

Next, we need to alter the database to reflect this password salt change. Inside a pod able to see the rucio postgres database, install a postgres client (`upgrade-rucio` in this example):

```
[root@upgrade-rucio tmp] yum install postgresql
```

Open a postgresql shell connected to the rucio postgres database:

```
[root@upgrade-rucio tmp]# psql --host rucio-postgresql -U rucio -d rucio -p 5432
```

where `rucio-postgresql` is the pod name.

Verify that the salted passwords were the same as those outputted from the server (optional):

```
select identity, password from test.identities;
```

And then updated corresponding identity entries with the password generated with the newer salting code, e.g. for `srcdev` identity a query like:

```
update test.identities SET password='<newer-salted-password>' WHERE identity='srcdev';
```

Lastly, remember to remove the `raise CannotAuthenticate` from authentication.py.
