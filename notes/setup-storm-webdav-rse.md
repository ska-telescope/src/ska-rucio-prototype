## Configuring StoRM as a token based https Rucio Storage Element (RSE)

Following https://github.com/italiangrid/storm-webdav to setup a StoRM RSE.

[Prerequisites](https://gitlab.com/ska-telescope/src/ska-rucio-prototype/-/blob/master/notes/setup-storm-webdav-rse.md#prerequisite)

[Prep](https://gitlab.com/ska-telescope/src/ska-rucio-prototype/-/blob/master/notes/setup-storm-webdav-rse.md#prep)

[Storage Config](https://gitlab.com/ska-telescope/src/ska-rucio-prototype/-/blob/master/notes/setup-storm-webdav-rse.md#storage-config)

[Rucio config](https://gitlab.com/ska-telescope/src/ska-rucio-prototype/-/blob/master/notes/setup-storm-webdav-rse.md#rucio-config)

[Test RSE behaviour](https://gitlab.com/ska-telescope/src/ska-rucio-prototype/-/blob/master/notes/setup-storm-webdav-rse.md#test-rse-behaviour)

[Notes](https://gitlab.com/ska-telescope/src/ska-rucio-prototype/-/blob/master/notes/setup-storm-webdav-rse.md#notes)

#### Prerequisite 
SL7 instance with 8 vCPU, 8GB RAM, 80 GB storage.

#### Prep
- Make IAM client 
    - Scopes: openid, profile, email, address, phone, offline_access, wlcg.groups
    - Grant types: authorization code, refresh, device
    - Redirect URIs: http://localhost:47056, http://localhost:8080, http://localhost:4242 
    - Save the ID, secret and RAT!
- Install dependencies and storm-webdav
```
sudo yum install java-11-openjdk-devel
sudo yum-config-manager --add-repo https://repo.cloud.cnaf.infn.it/repository/storm/storm-stable-centos7.repo
sudo yum install storm-webdav
```
#### Storage Config
- Make a storage area to expose (mkdir /data)
- Generate cert and key for the service to use 
    - Run
    `openssl req -newkey rsa:4096 -nodes -sha256 -keyout hostkey.pem -x509 -days 3650 -out hostcert.pem` 
    - Store them as expected by the fields `STORM_WEBDAV_CERTIFICATE_PATH` and `STORM_WEBDAV_PRIVATE_KEY_PATH` in `/etc/systemd/system/storm-webdav.service.d/storm-webdav.conf`
- The config is mostly spread across three files 
    - `/etc/systemd/system/storm-webdav.service.d/storm-webdav.conf`
    - `/etc/storm/webdav/config/application.yml`
    - `/etc/storm/webdav/sa.d/[your storm name].properties`
    
    Generate the last by making a copy of /etc/storm/webdav/sa.d/sa.properties.template
- Config files
    - Fields set in storm-webdav-test-sa.properties, (comments and unused values removed for readability in a README)
    ```
    name=sa
    rootPath=/data/sa
    accessPoints=/storm/sa
    orgs=https://iam-escape.cloud.cnaf.infn.it/

    anonymousReadEnabled=false
    voMapEnabled=false

    orgsGrantReadPermission=true
    orgsGrantWritePermission=true
    wlcgScopeAuthzEnabled=true
    ```

    - Fields set in application.yml

    ```
    oauth:
    enable-oidc: true
    issuers:
        - name: escape
          issuer: https://iam-escape.cloud.cnaf.infn.it/
    spring:
    security:
        oauth2:
        client:
            provider:
            escape:
                issuer-uri: https://iam-escape.cloud.cnaf.infn.it/
            registration:
            escape:
                provider: escape
                client-name: ska-storm-webdav
                client-id: <redacted>
                client-secret: <redacted>
                scope:
                - openid
                - profile
                - wlcg.groups
    storm:
    voms:
        trust-store:
        dir: ${STORM_WEBDAV_VOMS_TRUST_STORE_DIR:/etc/grid-security/certificates}
    ```
- The storm user is the user that is running the service and is handling requests, touching storage etc. So we add a shell for storm user and run chown and chgrp for storm for have access to all the locations it needs including: `/data`, `/etc/storm/webdav/`, `/etc/grid-security/storm-webdav/`
- Start the service with `sudo systemctl start storm-webdav`
    - Logs in `/var/log/storm/webdav/storm-webdav-server.log`
- Test liveness with (from repo README)
```
curl http://localhost:8085/actuator/health
curl http://localhost:8085/status/metrics?pretty=true 
```
- Expose the service for external https access (one option is via NGINX reverse proxy)
- Test access to storage by creating a token and passing it to the service using curl, Postman, davix, etc. More here: https://gitlab.com/ska-telescope/src/ska-rucio-prototype/-/blob/master/notes/enable-tokens-1.26.2.md#check-token-based-access-to-storage 

#### Rucio Config
- Create a new RSE as per step 18 under https://gitlab.com/ska-telescope/src/ska-rucio-prototype/-/tree/master/#add-storage 
- Add a flag for enabling OIDC support
`rucio-admin rse set-attribute --rse <RSE> --key oidc_support --value True `
- In later Rucio versions (since [this](https://github.com/rucio/rucio/issues/3254)), be sure to add the RSE protocol by including `third_party_copy_read` and `third_party_copy_write` fields.

Upon running `rucio-admin rse info <RSE>`, protocols' domain section should look something like `domains: '{"lan": {"read": 1, "write": 1, "delete": 1}, "wan": {"read": 1, "write": 1, "delete": 1, "third_party_copy": 1, "third_party_copy_read": 1, "third_party_copy_write": 1}}'`

#### Test RSE behaviour
- Upload:
`export FILE=`uuidgen` && echo "test" > $FILE && rucio -vvv upload --rse <RSE> --lifetime 3600 --scope testing --register-after-upload $FILE`
- Replicate:
`rucio add-rule –lifetime 3600 <DID> <copies> <dest RSE expression>`

#### Notes
- If routing through an NGINX proxy with a limited IP allowlist, be sure the services that need to communicate with the StoRM service can do so (including FTS)
- To debug, can use Postman to send requests to the StoRM service REST API directly
- https://jwt.io/ is a handy tool to inspect tokens
- Useful links:
    - Italian grid StoRM Github: https://github.com/italiangrid/storm
    - WebDAV REST API: https://italiangrid.github.io/storm/documentation/webdav-guide/3.0.0/
    - GFAL2 Github: https://github.com/cern-fts/gfal2-util
    - StoRM config settings: https://github.com/italiangrid/storm-webdav/blob/master/doc/storage-area-configuration.md 
