# "Admin" tokens in 1.28.6
## General notes

In this workflow, the token retrieved at authentication by the user using the IAM auth client (`auth_code` flow) is not used. Instead, Rucio server gets a token via the IAM rucio admin client using the OAuth2 `client_credentials` grant. This type of grant is for app-app communication. It doesn’t utilise the `offline_access` scope (as this scope is needed to generate refresh tokens, but refresh tokens aren’t required when the app has all the credentials it needs to generate access tokens whenever it wants). It also doesn’t make sense to have the `openid` scope, or groups, as these are related to users, which this type of grant doesn’t necessitate.

Knowing this, it is still possible to include the `openid` and `offline_access` scopes, although it’s ambiguous what this actually means in this flow. As of writing, it is necessary to have the `offline_access` scope enabled on the admin client, as the token-exchange done by FTS exchanges an access token for a refresh token (again, quite what this means I don’t know - this token was originally a `client_credentials`, it doesn’t make sense to have a refresh token), adding in `offline_access`. Although it is not necessary to include the `offline_access` scope in the original token request when authenticating with Rucio, it **is** necessary that the original client has it enabled:

https://github.com/indigo-iam/iam/blob/afde07c4d675fbc39129eb80ceb2a367b9c37a97/iam-login-service/src/main/java/it/infn/mw/iam/core/oauth/exchange/DefaultTokenExchangePdp.java#L101

Also worth noting is that  the audience is overwritten if there are wlcg.groups present in the token:

https://gitlab.cern.ch/fts/fts-rest-flask/-/blob/develop/src/fts3rest/fts3rest/lib/oauth2provider.py#L202

If there are, it sets the audience to `https://wlcg.cern.ch/jwt/v1/any`, otherwise `None`.

Subsequently, if the audience is None, it later changes this to the FTS client ID:

https://gitlab.cern.ch/fts/fts-rest-flask/-/blob/develop/src/fts3rest/fts3rest/lib/openidconnect.py#L142

So all information about the original audience is lost.

As a `client_credentials` grant has no groups information, it follows that the audience after FTS token-exchange will always be the FTS client ID.

## Snags and advice

- We had a missing access token lifetime (set to 0) in the FTS client. This resulted in the `exp` field of the exchanged token not being set. A lot of storages rejected this as an invalid token.

- The polling and reaping Rucio daemons were not configured to use the `oidc_account` correctly. This has been fixed upstream in 1.28.7.

- Storage sites need to allow the FTS client ID as an allowed audience field (see above).

- Kill off any lingering references to X.509 user proxies. We had an issue with credentials being mangled together at storage.

- Some storages use the wlcg.groups claim to match an incoming user to a storage namespace, e.g. dCache. This is an implementation issue, as groups are an OIDC concept, tied to a user and not an application (hence they don't exist in a token obtained via `client_credentials`). We've been told by the Rucio devs that the "user token" flow, whereby the user's token (gotten by `auth_code` grant & complete with groups information) is exchanged all the way through to storage (what we were doing previously) is not actually the preferred way, and will be deprecated in favour of using this "admin" token retrieved by `client_credentials` (and eventually using scope-based authZ).


