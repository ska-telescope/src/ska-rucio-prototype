## Stage Server and Daemons images in private registry

Since pulling images from Docker Hub could lead to pull rate limit issues, we explicitly pushed these images to a local VM running a Docker registry. This will also allow us to easily make changes if required and update the images on our local registry without affecting the upstream.

```
$ docker login -u <username> -p <password> 192.168.50.97:5000
$ docker pull rucio/rucio-daemons:release-1.28.0
$ docker tag rucio/rucio-daemons:release-1.28.0 192.168.50.97:5000/rucio/rucio-daemons:release-1.28.0
$ docker push 192.168.50.97:5000/rucio/rucio-daemons:release-1.28.0
$ docker pull rucio/rucio-server:release-1.28.0
$ docker tag rucio/rucio-server:release-1.28.0 192.168.50.97:5000/rucio/rucio-server:release-1.28.0
$ docker push 192.168.50.97:5000/rucio/rucio-server:release-1.28.0
```

## Create special new upgrade image + pod

Rucio’s schema version is stored in the `alembic_version` table. Following guidance from CMS, build an image that looks like the rucio-upgrade image here: https://github.com/dmwm/CMSKubernetes/tree/master/docker/rucio-upgrade after commenting out a few CMS specific lines (specifically, those adding the `cmstfc.py` script). You can add a line to the Dockerfile for this image to install the PostgreSQL client with:
```
yum install postgresql
```
Similarly, it may be useful to add `vim` into the image at this stage.

We also built and pushed this image to our local registry:
```
$ docker build --build-arg RUCIO_VERSION=1.28.0 -t 192.168.50.97:5000/rucio/rucio-upgrade:release-1.28.0 .
$ docker push 192.168.50.97:5000/rucio/rucio-upgrade:release-1.28.0
```

This image is stored on the `src-docker-registry` machine in Rohini's home:

```bash
rjoshi@src-docker-registry:~/CMSKubernetes/docker/rucio-upgrade$ echo $HOSTNAME
src-docker-registry
rjoshi@src-docker-registry:~/CMSKubernetes/docker/rucio-upgrade$ pwd
/home/rjoshi/CMSKubernetes/docker/rucio-upgrade
```

On the Rucio head node, build a deployment-managed pod based on this upgrade image with:
```
$ kubectl create deployment --image=192.168.50.97:5000/rucio/rucio-upgrade:release-1.28.0 rucio-upgrade -o yaml > upgrade-deployment.yaml
```

## Database Upgrade via Alembic

From within the `rucio-upgrade-...` pod, check the `alembic.ini` file's `sqlalchemy.url` field and update it to point to the Rucio DB.

```
$ kubectl exec -it rucio-upgrade-5b96d8c8cf-pvkgq -- /bin/bash
[root@rucio-upgrade-5b96d8c8cf-pvkgq tmp]# ls
alembic.ini  alembic.ini.j2  ks-script-DrRL8A  rucio.config.default.cfg  upgrade_image_start.sh  yum.log
[root@rucio-upgrade-5b96d8c8cf-pvkgq tmp]# vim alembic.ini
```

The Alembic revision should be in the DB table `test.alembic_version`, and can be checked with `alembic current`:
```
[root@rucio-upgrade-5b96d8c8cf-pvkgq tmp]# alembic current
INFO  [alembic.runtime.migration] Context impl PostgresqlImpl.
INFO  [alembic.runtime.migration] Will assume transactional DDL.
739064d31565
[root@rucio-upgrade-5b96d8c8cf-pvkgq tmp]# psql --host rucio-postgresql -U rucio -d rucio -p 5432
rucio=> SELECT * FROM test.alembic_version;
 version_num  
--------------
 739064d31565
```

If this is the case, the DB can be upgraded via:
```
[root@rucio-upgrade-5b96d8c8cf-pvkgq tmp]# alembic upgrade head
```

If the Alembic revision is not in the DB, a more manual DB upgrade can be done, as per the `upgrade-1.23-to-1.26.md` guide.

## Helm upgrade

These commands install/upgrade a Helm release named `server` in the `rucio-test` K8s namespace with Helm chart version 1.28.0.
```
# (Ensure Helm repos are up to date)
$ helm repo add rucio https://rucio.github.io/helm-charts
$ helm repo update
```

Update the override values for `image.repository` and `image.tag` in `server.yaml` to point to the new images pushed to the registry:
```
image:
  repository: 192.168.50.97:5000/rucio/rucio-server
  tag: release-1.28.0
```

```
helm upgrade --install server --namespace rucio-test rucio/rucio-server --version 1.28.0  -f server.yaml
```

For the `daemons` deployment, the version 1.28.0 Helm chart has some issues due to [recent](https://github.com/rcarpa/helm-charts/commit/dbb76eeb86e3845cfb7efadb44f03329f37a4dec) changes to the way Daemon configurations are managed. Therefore, we had to use an earlier Helm chart (`1.26.2`), but still point to the latest Docker image version for the pods to run.

Update the override values for `image.repository` and `image.tag` in `daemons.yaml` to point to the new images pushed to the registry:
```
image:
  repository: 192.168.50.97:5000/rucio/rucio-daemons
  tag: release-1.28.0
```

Ensure that the `idpsecrets` can be mounted in the daemon pods - see the `additionalSecrets` field in `daemons.yaml` for an example. (This can also be done manually or using a secret manager like Vault.)

```
helm upgrade daemons --namespace rucio-test rucio/rucio-daemons --version 1.26.2 -f daemons.yaml
```
