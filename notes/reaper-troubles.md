## Background
The reaper2 daemon (renamed to reaper from rucio version 1.26 onwards) is responsible for cleanup on RSEs by deleting file replicas with rules that have expired.
## Problem
Within the k8s rucio setup there is a special k8s secret named daemons-rucio-ca-bundle-reaper that is used by the reaper daemon. If this secret is not created properly, you might see errors in your reaper pod that look something like
```
2021-10-11 10:44:21,797	root	11	WARNING	reaper[1/2] Deletion NOACCESS of testing:100KB_051021T11.01.26 as gsiftp://fal-pygrid-xxxxxx.uk:2811/dpm/lancs.ac.uk/........100KB_051021T11.01.26 on LANCASTER: The requested service is not available at the moment.
Details: An unknown exception occurred.
Details: globus_ftp_control: gss_init_sec_context failed OpenSSL Error: s3_clnt.c:1264: in library: SSL routines, function ssl3_get_server_certificate: certificate verify failed globus_gsi_callback_module: Could not verify credential globus_gsi_callback_module: Can't get the local trusted CA certificate: Untrusted self-signed certificate in chain with hash 7ed47087
```

## Solution
The reaper daemon's special secret needs to be created from a folder with not only all of the CA files with their sanitised names but also their alphanumeric code file names. These are often symlinked on a VM but symlinks can get lost when creating the kubernetes secret. 


You can check by running something like 
```
rjoshi@k8s-dev-head:/opt/deployed/ska-rucio-prototype$ kubectl describe secrets daemons-rucio-ca-bundle-reaper | grep ad9
ad9d1b74.0:                                             1371 bytes
ad9d1b74.namespaces:                                    512 bytes
ad9d1b74.signing_policy:                                183 bytes
```

The CAs included in the secret should include all the CAs that have issued certificates for the RSEs that you have configured in your Rucio instance. 
