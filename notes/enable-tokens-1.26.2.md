
### Enabling token based Rucio transfers

[Create Clients](https://gitlab.com/ska-telescope/src/ska-rucio-prototype/-/blob/master/notes/enable-tokens-1.26.2.md#create-clients)

[Sync OIDC identities from IAM to Rucio](https://gitlab.com/ska-telescope/src/ska-rucio-prototype/-/blob/master/notes/enable-tokens-1.26.2.md#sync-oidc-identities-from-iam-to-rucio)

[Update server config (helm)](https://gitlab.com/ska-telescope/src/ska-rucio-prototype/-/blob/master/notes/enable-tokens-1.26.2.md#update-server-config-helm)

[Update daemons config (helm)](https://gitlab.com/ska-telescope/src/ska-rucio-prototype/-/blob/master/notes/enable-tokens-1.26.2.md#update-daemons-config-helm)

[Check token-based access to Rucio](https://gitlab.com/ska-telescope/src/ska-rucio-prototype/-/blob/master/notes/enable-tokens-1.26.2.md#check-token-based-access-to-rucio)

[Check token-based access to storage](https://gitlab.com/ska-telescope/src/ska-rucio-prototype/-/blob/master/notes/enable-tokens-1.26.2.md#check-token-based-access-to-storage)

[Check token-based FTS transfers](https://gitlab.com/ska-telescope/src/ska-rucio-prototype/-/blob/master/notes/enable-tokens-1.26.2.md#check-token-based-fts-transfers)

[Check token-based Rucio transfers](https://gitlab.com/ska-telescope/src/ska-rucio-prototype/-/blob/master/notes/enable-tokens-1.26.2.md#check-token-based-rucio-transfers)

[Notes](https://gitlab.com/ska-telescope/src/ska-rucio-prototype/-/blob/master/notes/enable-tokens-1.26.2.md#notes)

#### Create Clients

-   Using the IAM web portal, register two new clients: Rucio Auth Client and Rucio Admin IAM Client and retain the:
    

	-   Client_id,
    
	-   Client_secret,
    
	-   registration access token (RAT)
    

-   On each client, set up the redirect_uris to include:
    

	-   https://<your_rucio_server_url>/auth/oidc_token,
    
	-   https://<your_rucio_server_url>/auth/oidc_code
    

-   For the Rucio Auth Client:
    

	-   Enable following grants:
    

		-   token exchange (requires IAM administrator),
    
		-   token refresh,
    
		-   authorisation code,
    
		-   device code (see below 9a & 9b)
    

	-   Enable the scopes: openid, profile, email, address, phone, offline_access, storage.read:/, storage.modify:/, storage.create:/, wlcg.groups, fts, fts:submit-transfer, rucio
    

-   For the Rucio Admin IAM  Client: (TODO check for completion)
    

	-   Enable client_credentials grant
    
	-   Include scim:read scope ([requires IAM administrator](https://indigo-iam.github.io/v/current/docs/reference/api/scim-api/))
    

-   Create an idpsecrets.json file that stores client info in a way that Rucio will need (further below)
	-  Note placement of auth client credentials and admin client credentials inside the SCIM section.
    
	-  This file is very crucial, validate the json if needed and double check the content.
    
```
{

"escape": {

"redirect_uris": [

"https://srcdev.skatelescope.org/rucio/auth/oidc_token",

"https://srcdev.skatelescope.org/rucio/auth/oidc_code"

],

"registration_access_token": "<auth client RAT>",

"client_secret": "<auth client secret>",

"client_id": "<auth client id>",

"SCIM": {

"client_secret": "<admin client secret>",

"grant_type": "client_credentials",

"registration_access_token": "<admin client RAT>",

"client_id": "<admin client id>"

},

"issuer": "https://iam-escape.cloud.cnaf.infn.it/"

}

}
```
    

#### Sync OIDC identities from IAM to Rucio

1.  Add OIDC identities. 
Run the IAM sync script. This script uses the IAM admin client to access the information regarding IAM users and creates matching Rucio accounts for them mapping their IAM identities to Rucio identities. The SCIM:read scope on the admin client is what allows the necessary access.


    1.  Run this script from a location with both Rucio client and server libraries available. (upgrade-rucio pod for ex)
    
    2.  Similarly, the rucio.cfg is also a combination of a client-side rucio.cfg and a server one (needs DB access).
    
```
[client]

rucio_host = http://server-rucio-server

auth_host = http://server-rucio-server-auth

ca_cert = /etc/grid-security/certificates/5fca1cb1.0

auth_type = userpass

username = ddmlab

password = <secret>

account = root

client_cert = /opt/rucio/etc/usercert.pem

client_key = /opt/rucio/etc/userkey.pem

client_x509_proxy = $X509_USER_PROXY

request_retries = 3

  

[database]

default = postgresql://rucio:secret@rucio-postgresql/rucio

schema = test

pool_reset_on_return = rollback

echo = 0

pool_recycle = 600

  

[policy]

permission = generic

schema = generic

lfn2pfn_algorithm_default = hash

support = https://github.com/rucio/rucio/issues/

support_rucio = https://github.com/rucio/rucio/issues/
```
3.  Clone [https://github.com/ESCAPE-WP2/Utilities-and-Operations-Scripts](https://github.com/ESCAPE-WP2/Utilities-and-Operations-Scripts) and navigate to iam-rucio-sync directory
    
4.  Set appropriate env variables or set iam-sync.conf to
```
[IAM]

iam-server = https://iam-escape.cloud.cnaf.infn.it/
client-id = <IAM admin rucio client>

# Secret can be configure either here or using a seperate file

client-secret = <CLIENT_SECRET>
```

5.  Run the sync script    

`python3 sync_iam_rucio.py`

Check identities have been added:
```
[root@upgrade-rucio iam-rucio-sync]# rucio-admin account list-identities rjoshi

Identity: rjoshi,  type: USERPASS

Identity: SUB=f9d3db03-d1be-439f-8f2e-a87c72760f2c, ISS=https://iam-escape.cloud.cnaf.infn.it/,  type: OIDC

Identity: /C=UK/O=eScience/OU=Manchester/L=HEP/CN=rohini joshi,  type: X509
```
#### Update server config (helm)

2.  The idpsecrets.json file (from above) needs to available inside the server pods, this can be done either by baking in the json into the container image or creating a kubernetes secret.
    

	1.  To make a kubernetes secret for this  
    `kubectl create secret generic server-idpsecrets --from-file=idpsecrets.json`
    
	2.  TODO: check k8s secret flow for server on dev instance
    

3.  Add OIDC fields to the server.yaml. The expected_audience is not very well understood one that is to be set with an understanding with storage admins.
    

	1.  Yaml additions
    
```
config:

oidc:

idpsecrets: “/etc/idpsecrets/idpsecrets.json”

admin_issuer: "escape"

expected_audience: "[https://wlcg.cern.ch/jwt/v1/any](https://wlcg.cern.ch/jwt/v1/any)"

additionalSecrets:

idpsecrets:

secretName: idpsecrets

mountPath: /etc/idpsecrets/

  ```

2.  The default location for idpsecrets.json is /etc/idpsecrets.json so if copying in to the image without an override in the yaml, that is where to put it.
    
3.  If making the changes live, restart the server with httpd -k graceful
    

#### Update daemons config (helm)

4.  Run rucio-oauth-daemon. Update the daemons.yaml including a count for this daemon, set defaults by mirroring [ESCAPE setup](https://gitlab.cern.ch/escape-wp2/flux-rucio/-/blob/master/escape/rucio/releases/production/daemons.yaml#L201).  
```    
oauthManagerCount: 1
  

oauthManager:

threads: 1

podAnnotations: {}

resources:

limits:

memory: "200Mi"

cpu: "700m"

requests:

memory: "200Mi"

cpu: "700m"
```
  

5.  Create a kubernetes secret: `kubectl create secret generic daemons-idpsecrets --from-file=idpsecrets.json`
    
6.  Add OIDC fields to the daemons.yaml same as the server yaml. **In addition to that**, add
    
```
config:

conveyor:

request_oidc_scope: "fts:submit-transfer"

request_oidc_audience: "fts"
```

#### Check token-based access to Rucio

7.  Using a client pod/container check that you can authenticate with your IAM credentials
    

    1.  Delete any existing token with :
    

    `rm /tmp/user/.rucio_root/auth_token_root`

    2.  The rucio.cfg looks like (note rucio_host and auth_host are identical)
    
```
[client]

rucio_host = https://srcdev.skatelescope.org/rucio-dev

auth_host = https://srcdev.skatelescope.org/rucio-dev

ca_cert =

auth_type = oidc

username =

password =

account = rjoshi

client_cert = /opt/rucio/etc/usercert.pem

client_key = /opt/rucio/etc/userkey.pem

client_x509_proxy = $X509_USER_PROXY

request_retries = 3  
oidc_scope = openid profile wlcg.groups fts:submit-transfer

oidc_audience = fts https://wlcg.cern.ch/jwt/v1/any

  

[policy]

permission = generic

schema = generic

lfn2pfn_algorithm_default = hash

support = https://github.com/rucio/rucio/issues/

support_rucio = [https://github.com/rucio/rucio/issues/](https://github.com/rucio/rucio/issues/)

```

3.  Run a rucio command. Note "whoami" only works if the proxy is set to pass the "X-Requested-Host" header e.g. 

proxy_set_header X-Requested-Host $scheme://$host/rucio-dev;

otherwise you'll get a 404.

The flow that should follow is shown below
    
```
[user@client-ska-deployment-67bd568548-78wpf ~]$ rucio -S OIDC -a rjoshi --oidc-scope="openid profile" --oidc-audience="rucio" list-rses

  

Please use your internet browser, go to:

  

https://srcdev.skatelescope.org/rucio-dev/auth/oidc_redirect?jlWFgyR9EwYdJkKG6tuAivB

  

and authenticate with your Identity Provider.

Copy paste the code from the browser to the terminal and press enter:

yBWT<redacted>

  

AARNET_PER

AARNET_PER_ND

DESY_DCACHE

DESY_DCACHE_ND

IDIA

IDIA_ND

LANCASTER

LANCASTER_ND

MANCHESTER

MANCHESTER_ND
```

#### Check token-based access to storage

8.  Add `'oidc_support’: True` in attributes for rse (where relevant). Ask storage admins to enable token based access. They may require information regarding the claims and tokens that will come to storage for access. See 9.3 below. Update RSE prefix/port/protocol if they configure a separate area of storage or setup a new RSE as appropriate.
    
9.  OIDC Agent installation as per [https://wlcg-authz-wg.github.io/wlcg-authz-docs/token-based-authorization/oidc-agent/](https://wlcg-authz-wg.github.io/wlcg-authz-docs/token-based-authorization/oidc-agent/).
    

    1.  Be sure to add a few redirect URIs to the Rucio client config for this to work. 
        - http://localhost:47056
        - http://localhost:8080
        - http://localhost:4242
    
    2.  If running from a headless machine/remote server, be sure to add a ‘device’ grant type in the Rucio client config as well.
    
    3.  Once an account has been configured, running
        `oidc-token <account-name>` will give you a token. 

        A handy tool to inspect the token, specifically the audience and scopes is [https://jwt.io/](https://jwt.io/). A one-line command is  
        `oidc-token <account name> | cut -d. -f2 | base64 -d 2>/dev/null | jq .  `
        User claims can be seen with  
        `curl -s -H "Authorization: Bearer $(oidc-token ska-rucio-auth)" https://iam-escape.cloud.cnaf.infn.it/userinfo | jq .`
        Storage admins may need one or more of the above to set up things on their end.
    

10.  Check if this token provides access to the storage. Depending on the protocols provided by the storage, gfal or davix tools can be used for this.

        1.  CA certificates are important here. The CA path (whether implicit or explicit) needs the alphanumeric codes for the CAs that issued the certificate for the storage you are accessing. Based on where you are running this from, the files you need may/may not exist at `/etc/grid-security/certificates/`. This list of files may be incomplete.
    
        2.  export AT=`oidc-token <account name>`  
            `davix-ls -l --capath /etc/grid-security-certificates -H "Authorization: Bearer ${AT}" https://[hostname]:[port]/[prefix]`
    
        3.  export BEARER_TOKEN=`oidc-token <account name>`  
            gfal-ls…TODO
    
12.  Rucio upload

        1.  Upload to IMPERIAL with: 
            ```
            export FILE=`uuidgen` && echo "test" > $FILE && rucio upload --rse IMPERIAL --lifetime 3600 --scope testing --register-after-upload $FILE
            ```
    
        2.  Login to Rucio with your OIDC identity before attempting this (such that there is a valid auth token in place already)
    

#### Check token-based FTS transfers

12.  This step is optional but possibly helpful. Check a manual FTS transfer submission between two RSEs that are accepting tokens the way you expect them to.

        1.  To access the FTS REST CLI, spin up a container from the image `​​gitlab-registry.cern.ch/fts/fts-rest:latest`.

            Install the CLI tools with `yum install fts-rest-cli `
            `fts-rest-transfer-submit --access-token=${TOKEN} -s https://fts3-pilot.cern.ch:8446/ <src> <dest>  `
            Add `—insecure` flag if having trouble with certificates at /etc/grid-security/certificates (see 10.1)
    
#### Check token-based Rucio transfers

13.  Rucio issues noted in Note 2 and 3, manual tweaking in the daemons code required in the meantime. Some of these may go away in the latest version of Rucio (1.27.4 at the time of writing this) as work described so far has been done on 1.26.2. A lot of refactoring been done in the code base in this area. If on 1.26.2 and making following manual tweaks, refer to note 6 below. 
        1. Replace this line https://github.com/rucio/rucio/blob/1.26.2/lib/rucio/core/transfer.py#L101 with
            `ALLOW_USER_OIDC_TOKENS = True`
        2. Add following line at this location  https://github.com/rucio/rucio/blob/1.26.2/lib/rucio/core/oidc.py#L193 
            `auth_args["redirect_uri"] = None`
    
14.  The poller daemon requires a (even uglier) manual code hack as well, but something that may likely not be needed with a version upgrade.
    
15.  Initiate Rucio transfers between IMPERIAL and CNAF with  
```
export FILE=`uuidgen` && echo "test" > $FILE && rucio upload --rse IMPERIAL --lifetime 3600 --scope testing --register-after-upload $FILE && rucio add-rule testing:$FILE 1 CNAF
```
    

  

#### Notes:

1.  Rucio Bug: The OIDC methods assume that the Rucio server is located at the root of the domain. In [core/oidc.py](https://github.com/rucio/rucio/blob/master/lib/rucio/core/oidc.py#L310)  to build the url, we need to take into account subpaths. Live change was made to test the fix required, and then baked into the server image we are using. Issue [created.](https://github.com/rucio/rucio/issues/5220)
    
2.  User OIDC tokens are not used because the (allow_user_oidc_tokens) flag is not respected. Issue [created.](https://github.com/rucio/rucio/issues/5219)
    
3.  Rucio issue re: redirect_URI  
    TODO: expand on this issue
    
4.  Ingress change required to the auth-ingress resource. Correct metadata.annotations should be  
`nginx.ingress.kubernetes.io/rewrite-target: /auth/$2`

5.  Add cron to periodically map IAM users to rucio db accounts/identities using the same sync script as above. This will ensure new accounts are captured and any changes to identities are reflected.

6. How to manually tweak daemon code in a k8s Rucio setup


    1. Clone a copy of the helm charts locally
    2. Use kubectl logs to get a command for starting up the daemon. This may mean killing the pod and having it restart to have the command handy in the logs (it is normally at the start of the logs)
    3. Edit the entrypoint command in the helm chart template of the relevant daemon to be a sleep command. This allows you to  step into the pod, make live changes, then start up the pod
    4. Redeploy daemons helm chart
    5. kubectl exec into the pod and edit code in `/usr/local/lib/python3.6/site-packages/rucio/`
    6. Start the daemon with command from step 2.
