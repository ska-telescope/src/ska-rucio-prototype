## Background

Rucio is a data lake orchestrator developed by CERN. It maintains a database of where data is located in attached Rucio Storage Elements (RSEs) and, through a suite of daemons, monitors and updates the state of the data lake according to user requests. In addition to upload and deletion of data, Rucio brokers data transfers between sites.

Due to the complexity of the software stack itself and external services that must be configured to enable the various data lake capabilities, there is a multi-step setup process that leverages the Kubernetes (K8s) container orchestration service for deploying Rucio on cloud infrastructure.

This repository contains scripts and Helm charts to make the process of deploying Rucio more accessible.

## Rucio Deployment

### Prerequisites
- Machines to use for K8s cluster head and worker nodes with sudo access
- A robot certificate

### Setup

#### Enablers
1. Create K8s cluster (version 1.18.2) with c2.large head node, and 2 c3.medium worker nodes
Following steps here for cluster creation: https://github.com/rohinijoshi06/jupyterhub-on-k8s 
    1. `runOnHeadNode.sh`
    2. `setupCluster.sh`
    3. `runOnWorkerNode.sh`
    4. Join nodes to the cluster
    5. Alternatively, use a cluster provisioning tool (like Magnum). 
2. (Recommended) Enable use of private registry on the cluster. Our registry uses a self-signed certificate, so we will copy it to the head and worker nodes in order for it to be trusted.
    
    * Head node: Copy `domain.crt` to `/etc/docker/certs.d/192.168.50.97:5000/`
    
    * Worker nodes: Copy `domain.crt` to `/etc/ssl/certs/`, and reload Docker
        ```
        sudo cp domain.crt /etc/ssl/certs/
        sudo systemctl daemon-reload
        sudo systemctl restart docker
        ```
    
    * Authenticate with private registry on the worker nodes and copy to `/.docker/`.
        ```
        $ docker login -u <username> -p <password> 192.168.50.97:5000
        $ sudo mkdir /.docker
        $ sudo cp /home/<user>/.docker/config.json /.docker/
        ```
3. If default storage class is not available on your cluster, deploy NFS provisioner with persistence in a dedicated namespace. Persistence for NFS is via a HostPath volume on rucio-worker-1 in this case. (Update `nfsprovisioner.yaml` in the dev/prod directory with correct `nodeSelector` and update `nfs-mounted-hostpath-pv.yaml` in the same directory with the appropriate `spec.hostPath.path`)
    ```
    $ helm repo add stable https://charts.helm.sh/stable
    $ helm repo update
    $ kubectl create namespace nfsprovisioner
    $ helm install nfs stable/nfs-server-provisioner --namespace nfsprovisioner -f nfsprovisioner.yaml
    $ kubectl apply -f nfs-mounted-hostpath-pv.yaml
    ``` 
    Ensure pods are running and PVC has bound to the PV you just applied.

4. Create a separate K8s namespace for Rucio (DB, secrets, server, and daemons)

    ```
    $ kubectl create namespace rucio-test
    ```

5. Create password-less pem files from the Robot certificate and a long-lived X.509 proxy. These will be used to build the K8s secrets
    ```
    $ openssl pkcs12 -in mycert.p12 -clcerts -nokeys -out usercert.pem
    $ openssl pkcs12 -in mycert.p12 -nocerts -nodes -out userkey.pem
    ```

    Open an interactive shell into a pod or container that is running with the image rohinijoshi/rucio-client-escape:latest and run:

    ```
    $ voms-proxy-init --cert usercert.pem --key userkey.pem --voms skatelescope.eu
    ```

6. Create secrets needed for the deployment. These are  [TODO: identify the ones actually needed and used]

    ```
    $ bash createsecret.sh
    ```

7. Deploy a DB (postgres) with one replica

    ```
    $ helm repo add bitnami https://charts.bitnami.com/bitnami
    $ helm repo update
    $ helm upgrade --install rucio --namespace rucio-test bitnami/postgresql --set postgresqlDatabase=rucio --set postgresqlUsername=rucio --set postgresqlPassword=secret
    ```
8. Run the init-pod to bootstrap DB
    ```
    $ helm repo add rucio https://rucio.github.io/helm-charts
    $ helm repo update
    $ kubectl apply -f init-pod.yaml
    ```

    You can check the success by looking at the logs of this pod OR run the pgsql client as above and look at the contents of the rucio DB with  `\dt+ *.* ` 

    ```
    $ kubectl run pgsql-client --rm --tty -i --restart='Never' --image docker.io/bitnami/postgresql:11.10.0-debian-10-r24 --env="PGPASSWORD=secret" --command -- psql rucio --host rucio-postgresql -U rucio -d rucio -p 5432
    ```

#### Helm deploy the Rucio server and daemons

9. Deploy Rucio server with ingress disabled (to start with). Edit the server.yaml to disable ingress. A `server.yaml` can be found in the dev and prod directories. Use a version of the helm charts (with the flag `--version`) that corresponds to the rucio version you are specifying in server.yaml. 

    ```
    $ helm upgrade --install server --namespace rucio-test rucio/rucio-server --version 1.26.0  -f server.yaml
    ```

10. Deploy the Rucio daemons. A `daemons.yaml` can be found in the dev and prod directories. Use a version of the helm charts (with the flag `--version`) that corresponds to the rucio version you are specifying in daemons.yaml. 

    ```
    $ helm upgrade --install daemons --namespace rucio-test rucio/rucio-daemons --version 1.26.0 -f daemons.yaml
    ```

    To get hermes2 running requires modification of the Rucio config table. From any pod with admin privileges:
    ```
    $ kubectl exec -it client-ska -- /bin/bash
    $ rucio-admin config set --section hermes --option services_list --value elastic
    ``` 

11. Check if things are okay. The yaml can be found in dev or prod directories.

    ```
    $ kubectl apply -f client-ska-deployment.yaml 
    ```

    Jump in the pod (your pod name will be different):
    ```
    $ kubectl exec -it client-ska-deployment-f959448cc-95gdb -- /bin/bash
    ```

    Check root user access which will be via userpass credentials added in to the DB via the init-pod we ran earlier (rucio whoami)

#### Add Ingress and monitoring

12. Add a load balancer service. To set one up from scratch (e.g. if running on a blank K8s cluster), configure MetalLB according to the instructions at https://github.com/rohinijoshi06/jupyterhub-on-k8s#load-balancer-for-bare-metal-cluster. Can obtain the metallb resources (.yaml files) from that repository.

13. Configure Ingress controller, as described in more detail at: https://github.com/rohinijoshi06/jupyterhub-on-k8s/tree/master/ingress. The yaml can be found in dev or prod directories.
    ```
    kubectl apply -f deployIngressController.yaml
    ```

14. Add Ingress for rucio server services
    1. If ingressing through src-proxy, create a new route in `/etc/nginx/sites-available/default`. Can copy existing route for rucio, changing proxy-pass directive to point to new cluster IP and changing the location (e.g. /rucio/) hereafter {LOCATION}.
    2. Enable ingress and authIngress in [dev/prod]/server.yaml and create the following new annotations for both: nginx.ingress.kubernetes.io/ssl-redirect: "false"
    3. For authIngress, add a second annotation: nginx.ingress.kubernetes.io/rewrite-target: /$2 and change the path to be: path: /auth(/|$)(.*)
    4. Under both ingress and authIngress, add the host name for the proxy machine that will connect to Rucio, e.g.:
        ```
        hosts:
        - srcdev.skatelescope.org
        ```
    5. Upgrade the server deployment: `helm upgrade --install server --namespace rucio-test rucio/rucio-server --version 1.26.0  -f server.yaml`
    6. Manually edit the ingresses and remove reference to hosts in rules (should look something like spec: rules: - http rather than spec: rules: - host)
    7. Test with curl 130.246.214.144/{LOCATION}/ping & curl 130.246.214.144/{LOCATION}/auth/ping from a machine outside the network. This tests the full route (user > src-proxy > rucio-head). If this fails, you can also try directly from src-proxy (changing curl commands accordingly)

15. Add monitoring (of nodes to start with)
    1. Create a monitoring namespace 
        ```
        $ kubectl create namespace monitoring
        ```
    2. Deploy the prometheus stack (prometheus, grafana, node exporters, and CRDs) 
    https://github.com/prometheus-community/helm-charts/tree/main/charts/kube-prometheus-stack 
        ```
        $ helm install prometheus-stack -n monitoring prometheus-community/kube-prometheus-stack -f prometheus-values.yaml
        ```
    3. Set up an ssh tunnel (if needed) to view the grafana dashboard by looking at the port number for the grafana service with something like
    4. Login to Grafana with admin/prom-operator (edit yaml to change this)
        ```
        $ ssh -L 30290:localhost:30290 -Nfl rjoshi [external facing IP of the head node]
        ```
    5. If monitoring is enabled in the Rucio server deployment, daemon metrics will also be available. 

#### Add storage

16. Add robot credentials to root user
Jump into the client-ska pod (see 12b) and run
    ```
    $ rucio-admin identity add --account root --type X509 --id '/C=UK/O=eScience/OU=city/L=community/CN=full name/CN=Robot-Captain' --email srcdev@skatelescope.org
    ```

17. Identify/prep storage to use
    1. Once storage has been identified and access verified, create a separate directory to use for the Rucio instance with gfal tools.
    2. Setup credentials to run gfal with (inside client pod)
        ```
        $ voms-proxy-init --cert /opt/rucio/etc/usercert.pem --key /opt/rucio/etc/userkey.pem --voms skatelescope.eu
        ```
    3. To see the contents
        ```
        $ gfal-ls -la [protocol]://[hostname]/[prefix]
        ```
        For example:
        ```
        $ gfal-ls -lHa gsiftp://bohr...ac.uk/dpm/tier2.hep.manchester.ac.uk/home/skatelescope.eu/rucio/
        ```
    4. Use gfal-mkdir to create directories
18. Add RSEs
    1. Add a deterministic RSE
        ```
        $ rucio-admin rse add MANCHESTER
        ```

    2. Point the RSE to an FTS
        ```
        $ rucio-admin rse set-attribute --rse MANCHESTER --key fts --value https://fts3-pilot.cern.ch:8446
        ```

    3. Set Tape field to false

        ```
        $ rucio-admin rse set-attribute --rse MANCHESTER --key istape --value False
        ```

    4. Allow the root user (unlimited) access 

        ```
        $ rucio-admin account set-limits root MANCHESTER "infinity"
        ```

    5. Configure one or more protocols

        ```
        $ rucio-admin rse add-protocol --hostname bohr...ac.uk --scheme https --prefix '/dpm/tier2.hep.manchester.ac.uk/home/skatelescope.eu/rucio/rucio-ska-20210209' --port 443 --imp 'rucio.rse.protocols.gfal.Default' --domain-json '{"lan": {"read": 1, "write": 1, "delete": 1}, "wan": {"read": 1, "write": 1, "delete": 1, "third_party_copy_read": 1, "third_party_copy_write": 1}}' MANCHESTER
        ```

    6. If adding more than one, set the TPC priority accordingly. Also, if adding root protocol, the corresponding prefix requires an additional slash (/) at the beginning.

        ```
        $ rucio-admin rse add-protocol --hostname bohr...ac.uk --scheme gsiftp --prefix '/dpm/tier2.hep.manchester.ac.uk/home/skatelescope.eu/rucio/rucio-ska-20210209' --port 2811 --imp 'rucio.rse.protocols.gfal.Default' --domain-json '{"wan": {"read": 1, "write": 1, "delete": 1, "third_party_copy": 2}, "lan": {"read": 1, "write": 1, "delete": 1}}' MANCHESTER
        ```
    
    7. Add links to other RSEs (these are directional)

        ```
        $ rucio-admin rse add-distance LANCASTER MANCHESTER
        $ rucio-admin rse add-distance MANCHESTER LANCASTER
        ```

        or using script:

        ```
        # list all current RSEs and loop over them
        rucio list-rses > rses.txt
        for EXISTINGRSE in `cat rses.txt`
        do
                rucio-admin rse add-distance "$EXISTINGRSE" "$RSE"
                rucio-admin rse add-distance "$RSE" "$EXISTINGRSE"
        done
        ```

        in 1.29 this is different (otherwise you get "no path" errors in conveyor-submitter):

        ```
        # list all current RSEs and loop over them
        rucio list-rses > rses.txt
        for EXISTINGRSE in `cat rses.txt`
        do
                rucio-admin rse add-distance --ranking 1 "$EXISTINGRSE" "$RSE"
                rucio-admin rse add-distance --ranking 1 "$RSE" "$EXISTINGRSE"
                rucio-admin rse add-distance --distance 1 "$EXISTINGRSE" "$RSE"
                rucio-admin rse add-distance --distance 1 "$RSE" "$EXISTINGRSE"                
        done
        ```

        may need to verify that agis_distance and ranking columns are filled in the distances table:

        ```sql
        select r1.rse, r2.rse, ranking, agis_distance, geoip_distance FROM distances LEFT JOIN rses r1 on r1.id=src_rse_id LEFT JOIN rses r2 on r2.id=dest_rse_id;
        ```
#### Add data 

19. Upload a file. From inside the client pod:
    ```
    $ export FILE=`uuidgen` && echo "test">> $FILE && rucio -v upload --rse MANCHESTER --lifetime 86400 --scope root_test --register-after-upload $FILE
    ```

20. Replicate the file
    1. Following 18,
        ```
        $ rucio add-rule root_test:$FILE 1 MANCHESTER
        ```
    2. Check on the rule with 
        ```
        $ rucio list-rules root_test:$FILE
        ```

### Troubleshooting
If the rucio DB is missing and needs to be created manually:

Create a ‘rucio’ user, password secret with access to the ‘rucio’ DB
Run a client with 

```
$ kubectl run pgsql-client --rm --tty -i --restart='Never' --image docker.io/bitnami/postgresql:11.10.0-debian-10-r24 --env="PGPASSWORD=secret" --command -- psql rucio --host rucio-postgresql -U rucio -d rucio -p 5432
If you don't see a command prompt, try pressing enter.
```
Inside, run:
```
CREATE USER rucio WITH ENCRYPTED PASSWORD 'secret';
GRANT ALL PRIVILEGES ON DATABASE rucio TO rucio;
```
